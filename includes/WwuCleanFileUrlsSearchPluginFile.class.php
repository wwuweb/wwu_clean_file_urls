<?php

/**
 * @file
 * WwuCleanFileUrlsSearchPluginFile class.
 */

/**
 * WwuCleanFileUrlsSearchPluginFile.
 */
class WwuCleanFileUrlsSearchPluginFile extends LinkitSearchPluginFile {

  /**
   * Overrides createPath().
   */
  function createPath($entity) {
    $url_type = isset($this->conf['url_type']) ? $this->conf['url_type'] : $this->getDefaultUrlType();

    switch ($url_type) {
      case LINKIT_FILE_URL_TYPE_DIRECT:
        $wrapper = file_stream_wrapper_get_instance_by_uri($entity->uri);

        if ($wrapper instanceof DrupalLocalStreamWrapper) {
          $path = 'files/' . file_uri_target($entity->uri);
          $path = linkit_get_insert_plugin_processed_path($this->profile, $path, array('language' => (object) array('language' => FALSE)));
        }

        break;

      default:
        $path = parent::createPath($entity);
    }

    return $path;
  }

}
