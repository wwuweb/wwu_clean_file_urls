<?php

/**
 * @file
 * WwuCleanFileUrlsPublicStreamWrapper class.
 */

/**
 * WwuCleanFileUrlsPublicStreamWrapper.
 *
 * Overrides DrupalPublicStreamWrapper to simplify the externally accessible URL
 * of files.
 */
class WwuCleanFileUrlsPublicStreamWrapper extends DrupalPublicStreamWrapper {

  /**
   * Overrides getExternalUrl().
   *
   * Replaces the default file path with "files". Requires a server
   * configuration rewrite rule to map this clean path to the real site files
   * directory.
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());

    return $GLOBALS['base_url'] . '/files/' . drupal_encode_path($path);
  }

}
